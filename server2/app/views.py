from django.shortcuts import render
from subprocess import Popen
import wget
import pika
import requests
import time
import os



# Create your views here.
downloded = []
rcv = 0

def listen(request):
    startListening()


def startListening():
    cred = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters(host = "152.118.148.95", port = 5672, virtual_host = "/0806444524", credentials = cred)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1706022842_DIRECT", exchange_type="direct")

    routing_key = "harusunik"
    
    result = channel.queue_declare(queue='', exclusive=True)
    queue_name = result.method.queue

    channel.queue_bind(exchange="1706022842_DIRECT", queue=queue_name, routing_key=routing_key)
    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()

def callback(ch, method, properties, body):
    global rcv
    rcv += 1
    received = body.decode("utf-8").split(":")
    if ("http" in received[1]):
        url = received[1]+":"+received[2]
    else:
        url = received[1]

    
    def bar_custom(now,goal,x):
        msg = str(int(now / goal * 100))+"%"
        if(rcv <= 5):
            time.sleep(5)
        messageMQ(received[0]+" "+msg)
        print("isi nilai x: "+str(x))

    filename = wget.download(url, bar=bar_custom)
    
    downloded.append(filename)

    if received[0] == "10":
        passFiles()


def messageMQ(message):
    cred = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters(host = "152.118.148.95", port = 5672, virtual_host = "/0806444524", credentials = cred)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1706022842_TOPIC", exchange_type="topic")

    routing_key = "download"
    channel.basic_publish(exchange="1706022842_TOPIC", routing_key=routing_key, body=message)
    
    print(message)
    connection.close()


def passFiles():
    global downloded
    files = {}
    for i in range(0,len(downloded)):
        files["file" + str(i)] = open(downloded[i],'rb')
    requests.post('http://localhost:8003/', files=files)
    clearDownloaded()


def clearDownloaded():
    global downloded
    for i in range(0, len(downloded)):
        # remove the file
        p = Popen("rm '{}'".format(downloded[i]), shell=True)
        print("deleted file "+ downloded[i])
    downloded = []


