from django.shortcuts import render
import pika

# Create your views here.
def index(request):
    return render(request, "index.html")

def messageMQ(message):
    cred = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters(host = "152.118.148.95", port = 5672, virtual_host = "/0806444524", credentials = cred)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1706022842_DIRECT", exchange_type="direct")

    routing_key = "harusunik"
    channel.basic_publish(exchange="1706022842_DIRECT", routing_key=routing_key, body=message)
    
    print(message)
    connection.close()

def progress(request):
    response = {}
    if request.POST:
        var = "urln"
        resps = []
        download = "downloadn"
        compress = "compressn"
        for i in range(1,11):
            var = var[:-1]+str(i)
            download = download[:-1]+str(i)
            compress = compress[:-1]+str(i)
            url = request.POST[var]
            resp = {"url": url, "download":download, "compress":compress}
            messageMQ("{}:{}".format(i,url))
            resps.append(resp)
        response["urls"] = resps
    return render(request, "progress.html", response)
    