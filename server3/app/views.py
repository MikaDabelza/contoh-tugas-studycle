from django.shortcuts import render
from subprocess import Popen
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, FileResponse
import sys
import pika
if sys.version_info >= (3, 6):
    import zipfile
else:
    import zipfile36 as zipfile

# Create your views here.
listFiles = []


@csrf_exempt
def compress(request):
    global listFiles
    print(len(request.FILES))

    namaFile = 'filen'
    
    
    for i in range(0,len(request.FILES)):
        namaFile = namaFile[:-1]+str(i)
        filedonlot = request.FILES[namaFile]
        listFiles.append(filedonlot.name)
        handle_uploaded_file(filedonlot)
    compressFiles()
    return JsonResponse({"halo":"halo"})

def handle_uploaded_file(f):
    with open(f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
        print('sudah ada')


def compressFiles():
    global listFiles
    zipf = zipfile.ZipFile('Zipped_file.zip', 'w')
    for i in range (len(listFiles)):
        zipf.write(listFiles[i])
        messageMQ(str(int(i/len(listFiles)*100)),"compression")
        print(str(int(i/len(listFiles)*100)))
    zipf.close()
    clearDownloaded()
    messageMQ(str(100), "compression")
    #messageMQ("http://localhost:8003/download","link")
    sendEncrypted()
    
def downloadCompressed(request):
    file_name = "Zipped_file.zip"
    file = open(file_name,'rb')
    return FileResponse(file)

    
def clearDownloaded():
    global listFiles
    
    for i in range(0, len(listFiles)):
        # remove the file
        p = Popen("rm '{}'".format(listFiles[i]), shell=True)
        print("deleted file "+ listFiles[i])
    listFiles = []
    namaFile = 'filen'
        
def messageMQ(message,topic):
    cred = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters(host = "152.118.148.95", port = 5672, virtual_host = "/0806444524", credentials = cred)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1706022842_TOPIC", exchange_type="topic")

    routing_key = "download"
    channel.basic_publish(exchange="1706022842_TOPIC", routing_key=topic, body=message)
    
    print(message)
    connection.close()

import base64
import hashlib
import calendar
import datetime

def sendEncrypted():
    secret = "bambang"
    url = "/download/"

    future = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)
    expiry = calendar.timegm(future.timetuple())

    secure_link = "{key}{url}{expiry}".format(key=secret,
                                            url=url,
                                            expiry=expiry)
    secure_link = secure_link.encode('utf-8')
    hash = hashlib.md5(secure_link).digest()
    encoded_hash = base64.urlsafe_b64encode(hash).rstrip(b'=')

    messageMQ("http://localhost:8004/download/" + "?st=" + encoded_hash.decode('utf-8') + "&e=" + str(expiry), "link")
