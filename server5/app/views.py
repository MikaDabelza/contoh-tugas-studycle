from django.shortcuts import render

import datetime, time
import pika
# Create your views here.
def timeInit(request):
    startTime()
    
def startTime():
    print("Time started")
    while True:
        curr = datetime.datetime.now() + datetime.timedelta(hours=7)
        curr = curr.strftime('%H:%M:%S')
        messageMQ(str(curr),"time")
        time.sleep(1)

def messageMQ(message,topic):
    cred = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters(host = "152.118.148.95", port = 5672, virtual_host = "/0806444524", credentials = cred)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1706022842_TOPIC", exchange_type="topic")

    routing_key = "download"
    channel.basic_publish(exchange="1706022842_TOPIC", routing_key=topic, body=message)
    connection.close()